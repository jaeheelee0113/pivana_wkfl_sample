Welcome to Analysis Workflow's documentation!
======================================

.. toctree::
   :maxdepth: 5
   :caption: Table of Contents

   master
   vis
   util
   authors

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
