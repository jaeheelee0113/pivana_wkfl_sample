Visualization API
=================

.. automodule:: vis

.. autoclass:: Bar
	:members:

.. autoclass:: Pie
	:members: